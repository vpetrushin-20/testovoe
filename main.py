from enum import Enum
from typing import List

from fastapi import FastAPI
from pydantic import BaseModel, Field

app = FastAPI(
    title='Gym App'
)

gym_users=[
    {'id':1,'name':'Oleg','gender':'male'},
    {'id':2,'name':'Ekaterina','gender':'female'},
    {'id':3,'name':'Dmitry','gender':'male'},
    {'id':4,'name':'Vadim','gender':'male'},
    {'id':5,'name':'Olga','gender':'female'},
    {'id':6,'name':'Elizaveta','gender':'female'}
]


class User(BaseModel):
    id:int=Field(ge=1)
    name:str
    gender:str


@app.get('/users')
def get_users():
    return gym_users



@app.get('/users/{user_gender}')
def get_user_gender(user_gender:str , limit:int=1):
    return [user_1 for user_1 in gym_users if user_1.get('gender')==user_gender ][:limit]



@app.post('/users')
def add_user(users:List[User]):
    gym_users.extend(users)
    return {'Status':200,'data':gym_users}









